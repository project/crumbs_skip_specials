# Crumbs Skip Specials

This module is for sites that use both of:

* [Crumbs](https://www.drupal.org/project/crumbs)
* [Special Menu Items](https://www.drupal.org/project/special_menu_items)

And have a need to NOT display the special menu items in the breadcrumbs.

Note that you may not need this module if you can just drag the "menu.hierarchy.*" plugin into the "Disabled" section. However this is for the case where you do want menu hierarchy to be enabled, but do not want any special menu links to show up.
